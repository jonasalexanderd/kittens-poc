import { StyleSheet } from 'react-native';
import { verticalScale } from 'react-native-size-matters';

// Resources
import { Style } from './Style';
import * as Color from 'resources/values/color';

const ButtonSize = Object.freeze({
  BUTTON_32: verticalScale(32),
  BUTTON_40: verticalScale(40),
  BUTTON_50: verticalScale(50),
  BUTTON_54: verticalScale(54),
  BUTTON_60: verticalScale(60),
  BUTTON_65: verticalScale(65),
});

const ButtonSizeDefault = ButtonSize.BUTTON_54;
const borderRadius = 4;
const borderWidth = 3;
const Buttons = StyleSheet.create({
  back: {
    tintColor: 'black',
    height: ButtonSize.BUTTON_40,
    width: ButtonSize.BUTTON_40,
  },
  backgroundBlack: {
    backgroundColor: Color.black,
  },
  backgroundLightGrey: {
    backgroundColor: Color.lightGrey,
  },
  backgroundWhite: {
    backgroundColor: Color.white,
  },
  buttonBlack: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.black,
    paddingVertical: 0,
  },
  buttonBorderRadius: {
    borderRadius,
  },
  buttonError: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.error,
    paddingVertical: 0,
    margin: 0,
  },
  buttonGrayDisabled: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.buttonDisabled,
    paddingVertical: 0,
  },
  buttonGrayGrayBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.gray,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonGrayGrayBorderDisabled: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.buttonDisabled,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonPrimary: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.primary,
    paddingVertical: 0,
    margin: 0,
  },
  buttonPrimaryDisabled: {
    backgroundColor: Color.buttonPrimaryDisabled,
  },
  buttonSize32: {
    height: ButtonSize.BUTTON_32,
  },
  buttonSize40: {
    height: ButtonSize.BUTTON_40,
  },
  buttonTransparent: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.transparent,
    paddingVertical: 0,
  },
  buttonTransparentPrimaryBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.transparent,
    borderWidth,
    borderColor: Color.primary,
    paddingVertical: 0,
  },
  buttonTransparentWhiteBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.transparent,
    borderWidth,
    borderColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhite: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhiteAlpha01: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    paddingVertical: 0,
  },
  buttonWhiteBlackBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 1,
    borderColor: Color.black,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhiteGrayBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.gray,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhiteThirdTextBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 1,
    borderRadius,
    borderColor: Color.thirdText,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhiteThirdTextBorder40: {
    width: '100%',
    height: ButtonSize.BUTTON_40,
    borderWidth: 1,
    borderRadius,
    borderColor: Color.thirdText,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhitePrimaryBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.primary,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonWhitePrimaryBorderDisabled: {
    borderWidth: 0,
    backgroundColor: Color.buttonDisabled,
    paddingVertical: 0,
  },
  buttonWhiteRedBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.red,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonYellow: {
    width: '100%',
    height: ButtonSizeDefault,
    backgroundColor: Color.yellow,
    paddingVertical: 0,
  },
  buttonYellowBorder: {
    width: '100%',
    height: ButtonSizeDefault,
    borderWidth: 2,
    borderRadius,
    borderColor: Color.yellow,
    backgroundColor: Color.white,
    paddingVertical: 0,
  },
  buttonText: {
    fontSize: Style.FONT_SIZE,
  },
  buttonTextBold: {
    fontSize: Style.FONT_SIZE,
    fontWeight: '800',
  },
  indicator: {
    borderWidth: 1,
    borderColor: Color.black,
    backgroundColor: Color.white,
  },
  buttonWidth15: {
    width: Style.DEVICE_WIDTH / 1.5,
  },
  buttonWidthFull: {
    width: Style.DEVICE_WIDTH,
  },
  containerButtonWidth50: {
    width: '50%',
    marginLeft: 0,
    marginRight: 0,
  },
  containerButtonWidth60: {
    width: '60%',
    marginLeft: 0,
    marginRight: 0,
  },
  containerButtonWidth75: {
    width: '75%',
    marginLeft: 0,
    marginRight: 0,
  },
  containerButtonWidth100: {
    width: '100%',
    marginLeft: 0,
    marginRight: 0,
  },
});

export { ButtonSize, ButtonSizeDefault, Buttons };
