/* eslint import/prefer-default-export: 0 */

import { StyleSheet } from 'react-native';

// Resources
import { Style } from './Style';
import {
  fontTextBold,
  fontTextRegular,
  fontTextSemiBold,
  fontTitleBold,
  fontTitleExtraBold,
  fontTitleRegular,
} from 'resources/styles/TextsCustomFonts';
import * as Color from 'resources/values/color';

const Texts = StyleSheet.create({
  alignCenter: {
    textAlign: 'center',
  },
  alignLeft: {
    textAlign: 'left',
  },
  alignRight: {
    textAlign: 'right',
  },
  fontFamilyTextBold: {
    fontFamily: fontTextBold(),
  },
  fontFamilyTextRegular: {
    fontFamily: fontTextRegular(),
  },
  fontFamilyTextSemiBold: {
    fontFamily: fontTextSemiBold(),
  },
  fontFamilyTitleBold: {
    fontFamily: fontTitleBold(),
  },
  fontFamilyTitleExtraBold: {
    fontFamily: fontTitleExtraBold(),
  },
  fontFamilyTitleRegular: {
    fontFamily: fontTitleRegular(),
  },
  navBarDrawer: {
    fontSize: Style.FONT_SIZE_SMALL_S,
    color: Color.second,
    fontFamily: fontTitleRegular(),
  },
  subTitle: {
    fontSize: Style.FONT_SIZE_TITLE,
    color: Color.primaryText,
    fontWeight: '400',
    marginBottom: 10,
  },
  textColorBlack: {
    color: Color.black,
  },
  textColorBlue: {
    color: Color.primary,
  },
  textColorError: {
    color: Color.error,
  },
  textColorGray: {
    color: Color.gray,
  },
  textColorGrayButtonDisabled: {
    color: Color.buttonDisabled,
  },
  textColorGrayDisabled: {
    color: Color.textDisabled,
  },
  textColorGrayLight: {
    color: Color.lightGrey,
  },
  textColorGreen: {
    color: Color.green,
  },
  textColorPrimary: {
    color: Color.primary,
  },
  textColorPrimaryText: {
    color: Color.primaryText,
  },
  textColorRed: {
    color: Color.red,
  },
  textColorSecond: {
    color: Color.second,
  },
  textColorSecondText: {
    color: Color.secondText,
  },
  textColorTextDisabled: {
    color: Color.textDisabled,
  },
  textColorThirdText: {
    color: Color.thirdText,
  },
  textColorWhite: {
    color: Color.white,
  },
  textColorYellow: {
    color: Color.yellow,
  },
  textError: {
    fontSize: Style.FONT_SIZE_SMALL,
    fontWeight: 'normal',
    color: Color.error,
    fontFamily: fontTextRegular(),
  },
  textFontWeightBold: {
    fontWeight: 'bold',
    fontFamily: fontTextSemiBold(),
  },
  textFontWeightNormal: {
    fontWeight: 'normal',
    fontFamily: fontTextRegular(),
  },
  textNormal: {
    fontSize: Style.FONT_SIZE,
    fontWeight: 'normal',
    color: Color.primaryText,
    fontFamily: fontTextRegular(),
  },
  textLWithOutColor: {
    fontSize: Style.FONT_SIZE,
    fontWeight: 'normal',
    fontFamily: fontTextRegular(),
  },
  textSmall: {
    fontSize: Style.FONT_SIZE_SMALL,
    color: Color.primaryText,
    fontFamily: fontTextRegular(),
  },
  textSmallS: {
    fontSize: Style.FONT_SIZE_SMALL_S,
    color: Color.primaryText,
    fontFamily: fontTextRegular(),
  },
  textSmallXS: {
    fontSize: Style.FONT_SIZE_SMALL_XS,
    color: Color.primaryText,
    fontFamily: fontTextRegular(),
  },
  textSubTitle: {
    fontSize: Style.FONT_SIZE,
    fontWeight: 'normal',
    color: Color.primaryText,
    fontFamily: fontTitleRegular(),
  },
  title: {
    fontSize: Style.FONT_SIZE_TITLE,
    color: Color.primaryText,
    fontWeight: '800',
    fontFamily: fontTitleBold(),
  },
  titleForm: {
    textAlign: 'center',
    marginTop: 20,
    fontFamily: fontTitleBold(),
  },
  titleS: {
    fontSize: Style.FONT_SIZE_TITLE_S,
    color: Color.primaryText,
    fontWeight: '800',
    marginBottom: 10,
    fontFamily: fontTitleBold(),
  },
  titleM: {
    fontSize: Style.FONT_SIZE_TITLE_M,
    color: Color.primaryText,
    fontWeight: '800',
    marginBottom: 10,
    fontFamily: fontTitleBold(),
  },
  titleL: {
    fontSize: Style.FONT_SIZE_TITLE_L,
    color: Color.primaryText,
    fontWeight: '800',
    marginBottom: 10,
    fontFamily: fontTitleBold(),
  },
  titleXL: {
    fontSize: Style.FONT_SIZE_TITLE_XL,
    color: Color.primaryText,
    fontWeight: '800',
    marginBottom: 10,
    fontFamily: fontTitleBold(),
  },
});

export { Texts };
