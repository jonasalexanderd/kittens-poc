const TEXT_BOLD = 'Your-Custom-Font-Bold';
const TEXT_REGULAR = 'Your-Custom-Font-Regular';
const TEXT_SEMI_BOLD = 'Your-Custom-Font-SemiBold';
const TITLE_BOLD = 'Your-Custom-Title-Bold';
const TITLE_EXTRA_BOLD = 'Your-Custom-Title-ExtraBold';
const TITLE_REGULAR = 'Your-Custom-Title-Regular';

const CUSTOM_FONTS = false;

export const fontTextBold = () => (CUSTOM_FONTS === true ? TEXT_BOLD : 'system font');

export const fontTextRegular = () => (CUSTOM_FONTS === true ? TEXT_REGULAR : 'system font');

export const fontTextSemiBold = () => (CUSTOM_FONTS === true ? TEXT_SEMI_BOLD : 'system font');

export const fontTitleBold = () => (CUSTOM_FONTS === true ? TITLE_BOLD : 'system font');

export const fontTitleExtraBold = () => (CUSTOM_FONTS === true ? TITLE_EXTRA_BOLD : 'system font');

export const fontTitleRegular = () => (CUSTOM_FONTS === true ? TITLE_REGULAR : 'system font');
